import 'package:ezv_app_tes/app/routes/app_pages.dart';
import 'package:ezv_app_tes/design/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

void main() {
  runApp(
    ScreenUtilInit(
      useInheritedMediaQuery: true,
      minTextAdapt: true,
      builder: (context, child) => GetMaterialApp(
        title: 'EZV Test App',
        theme: appTheme(context),
        initialRoute: AppPages.INITIAL,
        getPages: AppPages.routes,
      ),
    ),
  );
}
