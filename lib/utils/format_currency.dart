import 'package:intl/intl.dart';

String priceFormat(dynamic price, {bool show = true, int decimal = 0}) {
  if (price != null) {
    if (price.runtimeType == String) {
      return NumberFormat.currency(
        locale: 'en_US',
        symbol: show ? r'$ ' : '',
        decimalDigits: decimal,
      ).format(double.parse(price));
    }
    return NumberFormat.currency(
      locale: 'en_US',
      symbol: show ? r'$ ' : '',
      decimalDigits: decimal,
    ).format(price);
  } else {
    return 'Rp. Null';
  }
}

String sliceDate(String num) {
  final a = num.substring(5, 10);
  final bln = a.substring(0, 2);
  final tgl = a.substring(3, 5);
  final c = '$tgl/$bln';
  return c;
}
