import 'dart:developer';

import 'package:flutter/foundation.dart';

logApp(String s) {
  if (kDebugMode) {
    log(s);
  }
}
