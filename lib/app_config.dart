import 'package:ezv_app_tes/design/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

// Default button values

const dButtonColor = Color.fromARGB(255, 3, 167, 132);
const dButtonElevation = 2.0;
const dButtonPadding = EdgeInsets.symmetric(vertical: 8, horizontal: 16);
const dButtonRadius = 6.0;
TextStyle get dButtonText => TS.titleSmall.copyWith(color: Colors.white);

//Text default setting
String fontFamilyApp = GoogleFonts.montserrat().fontFamily ?? 'Roboto';

//Textfield config
const dTextColor = Color(0xFF1A1A1A);

//Default Card Settings
final dCardRadius = Corners.sm;
const dCardBorderWidth = 1.0;
const dCardColor = Colors.white;
const dCardOutlineColor = Colors.grey;
const dCardIsOutline = false;
final dShadows = [
  const BoxShadow(
    offset: Offset(1, 1),
    blurRadius: 3,
    color: Color.fromARGB(255, 194, 194, 194),
    spreadRadius: 1,
  )
];
final dCardMargin = Paddings.med;

//Default Appbar settings
final dAppbarHeight = 60.h;
