// ignore_for_file: constant_identifier_names, unnecessary_lambdas

import 'package:ezv_app_tes/app/models/product_model.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart' as sql;

class DatabaseHelper {
  static const TABLE_LIKED = 'trx_table';

  static Future<void> createTables(sql.Database database) async {
    await database.execute('''
CREATE TABLE $TABLE_LIKED(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        id_liked INTEGER
        
      )
      ''');
  }
// id: the id of a item
// title, description: name and description of your activity
// created_at: the time that the item was created. It will be automatically handled by SQLite

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'ezv2023.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  // Create new item
  static Future<int> saveLiked(ProductModel item) async {
    final db = await DatabaseHelper.db();

    final id = await db.insert(
      TABLE_LIKED,
      item.toJson2(),
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );

    return id;
  }

  // Read all items
  static Future<List<ProductModel>> getAllLiked() async {
    final db = await DatabaseHelper.db();
    final List data = await db.query(
      TABLE_LIKED,
    );
    return List.from(data.map((e) => ProductModel.fromJson2(e)));
  }

  static Future<void> deleteLiked(int id) async {
    final db = await DatabaseHelper.db();
    try {
      await db.delete(TABLE_LIKED, where: 'id_liked = ?', whereArgs: [id]);
    } catch (err) {
      debugPrint('Something went wrong when deleting an item: $err');
    }
  }
}
