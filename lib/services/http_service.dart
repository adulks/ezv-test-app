// ignore_for_file: constant_identifier_names

import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ezv_app_tes/utils/utils.dart';

enum Method { POST, GET, PUT, DELETE, PATCH }

class HttpService {
  static Dio? _dio;

  static header() => {'Content-Type': 'application/json'};

  Future<HttpService> init() async {
    _dio = Dio(BaseOptions(headers: header()));
    initInterceptors();
    return this;
  }

  static void initInterceptors() {
    _dio?.interceptors.add(
      InterceptorsWrapper(
        onRequest: (requestOptions, handler) {
          logApp(
            'REQUEST[${requestOptions.method}] => PATH: ${requestOptions.path}'
            '=> REQUEST VALUES: ${requestOptions.queryParameters} => HEADERS: ${requestOptions.headers}',
          );
          return handler.next(requestOptions);
        },
        onResponse: (response, handler) {
          logApp(
            'RESPONSE[${response.statusCode}] => DATA: ${jsonEncode(response.data)}',
          );
          return handler.next(response);
        },
        onError: (err, handler) {
          logApp('Error[${err.response?.statusCode}]');
          return handler.next(err);
        },
      ),
    );
  }

  static Future<dynamic> request({
    required String url,
    required Method method,
    Map<String, dynamic>? params,
    bool withToken = false,
  }) async {
    logApp('url : $url');
    if (params != null) {
      logApp('params : ${jsonEncode(params)}');
    }
    Response response;

    _dio = Dio(BaseOptions(headers: header()));

    initInterceptors();

    try {
      if (method == Method.POST) {
        response = await _dio!.post(url, data: params);
      } else if (method == Method.DELETE) {
        response = await _dio!.delete(url);
      } else if (method == Method.PATCH) {
        response = await _dio!.patch(url);
      } else if (method == Method.PUT) {
        response = await _dio!.put(url, data: params);
      } else {
        response = await _dio!.get(url, queryParameters: params);
      }

      if (response.statusCode == 200) {
        return response.data;
      } else if (response.statusCode == 401) {
        throw Exception('Unauthorized');
      } else if (response.statusCode == 500) {
        throw Exception('Server Error');
      } else {
        throw Exception("Something does wen't wrong");
      }
    } on SocketException catch (e) {
      logApp(e.toString());
      throw Exception('Not Internet Connection');
    } on FormatException catch (e) {
      logApp(e.toString());
      throw Exception('Bad response format');
    } on DioError catch (e) {
      if (e.type == DioErrorType.badResponse) {
        final response = e.response;
        try {
          if (response != null && response.data != null) {
            final data = json.decode(response.data as String) as Map;

            throw Exception(data['message'] as String);
          }
        } catch (e) {
          throw Exception('Internal Error');
        }
      } else if (e.type == DioErrorType.connectionTimeout ||
          e.type == DioErrorType.receiveTimeout ||
          e.type == DioErrorType.sendTimeout) {
        throw Exception('Request timeout');
      } else if (e.error is SocketException) {
        throw Exception('No Internet Connection!');
      }
    } catch (e) {
      rethrow;
    }
  }
}
