import 'package:ezv_app_tes/app/models/product_model.dart';
import 'package:get/get.dart';

class DetailProductController extends GetxController {
  ProductModel product = ProductModel(images: []);

  final count = 0.obs;
  @override
  void onInit() {
    final args = Get.arguments;
    product = args;
    super.onInit();
  }
}
