import 'package:cached_network_image/cached_network_image.dart';
import 'package:ezv_app_tes/app/modules/detail_product/controllers/detail_product_controller.dart';
import 'package:ezv_app_tes/design/colors.dart';
import 'package:ezv_app_tes/design/styles.dart';
import 'package:ezv_app_tes/utils/format_currency.dart';
import 'package:ezv_app_tes/widgets/page_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class DetailProductView extends GetView<DetailProductController> {
  const DetailProductView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return PageBase(
      title: 'Detail Product',
      child: Container(
        padding: Paddings.med,
        color: bgColor,
        child: Column(
          children: [
            CachedNetworkImage(
              imageUrl: controller.product.thumbnail,
              fit: BoxFit.cover,
              width: 1.sw,
              height: .2.sh,
            ),
            12.verticalSpace,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        controller.product.title,
                        style: TS.titleLarge,
                      ),
                      Text(
                        'Stock : ${priceFormat(controller.product.stock, show: false)}',
                        style: TS.bodySmall,
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      priceFormat(controller.product.price),
                      style: TS.labelMedium.copyWith(
                        decoration: TextDecoration.lineThrough,
                        color: primaryColor,
                      ),
                    ),
                    Text(
                      priceFormat(
                        controller.product.price -
                            ((controller.product.discountPercentage / 100) *
                                controller.product.price),
                      ),
                      style: TS.titleLarge.copyWith(color: Colors.green),
                    ),
                  ],
                ),
              ],
            ),
            12.verticalSpace,
            AbsorbPointer(
              child: RatingBar.builder(
                initialRating: controller.product.rating,
                minRating: 1,
                allowHalfRating: true,
                itemPadding: const EdgeInsets.symmetric(horizontal: 4),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {},
              ),
            ),
            16.verticalSpace,
            Text(
              controller.product.description,
              style: TS.bodySmall,
            ),
            20.verticalSpace,
            Expanded(
              child: GridView.count(
                crossAxisSpacing: 8,
                mainAxisSpacing: 8,
                crossAxisCount: 3,
                children: controller.product.images
                    .map(
                      (e) => CachedNetworkImage(
                        imageUrl: e,
                        fit: BoxFit.cover,
                      ),
                    )
                    .toList(),
              ),
            )
          ],
        ),
      ),
    );
  }
}
