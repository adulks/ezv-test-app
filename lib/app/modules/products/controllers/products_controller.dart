import 'package:ezv_app_tes/app/models/product_model.dart';
import 'package:ezv_app_tes/app/routes/app_pages.dart';
import 'package:ezv_app_tes/services/database_helper.dart';
import 'package:ezv_app_tes/services/http_service.dart';
import 'package:ezv_app_tes/utils/utils.dart';
import 'package:ezv_app_tes/widgets/show_dialog.dart';
import 'package:get/get.dart';

class ProductsController extends GetxController {
  RxBool isLoading = false.obs;
  RxList<ProductModel> listProduct = <ProductModel>[].obs;
  RxList<ProductModel> listLiked = <ProductModel>[].obs;

  Future<void> getProducts() async {
    isLoading.value = true;
    try {
      final r = await HttpService.request(
        url: 'https://dummyjson.com/products',
        method: Method.GET,
      );
      final List data = r['products'];
      final temp = <ProductModel>[];
      for (final l in data) {
        final m = ProductModel.fromJson(l);
        m.isLiked = isLiked(m.id);
        temp.add(m);
      }
      listProduct(
        temp,
      );
      await getLiked();
      isLoading.value = false;
    } catch (e) {
      isLoading.value = false;
      showPopUpInfo(title: 'Informasi', description: e.toString());
    }
  }

  Future<void> showDetail(ProductModel item) async {
    await Get.toNamed(Routes.DETAIL_PRODUCT, arguments: item);
  }

  @override
  void onInit() {
    getProducts();

    super.onInit();
  }

  Future<void> getLiked() async {
    final r = await DatabaseHelper.getAllLiked();
    listLiked(r);
  }

  bool isLiked(int id) {
    for (final l in listLiked) {
      if (l.id == id) {
        return true;
      }
    }
    return false;
  }

  Future<void> onTapLike(ProductModel item) async {
    await DatabaseHelper.saveLiked(item);
    listProduct.where((p0) => p0.id == item.id).first.isLiked = true;
    await getProducts();
  }

  Future<void> like(ProductModel item) async {
    logApp('${item.isLiked}');
    if (item.isLiked) {
      await onTapDisLike(item);
    } else {
      await onTapLike(item);
    }
  }

  Future<void> onTapDisLike(ProductModel item) async {
    await DatabaseHelper.deleteLiked(item.id);
    listProduct.where((p0) => p0.id == item.id).first.isLiked = false;
    await getProducts();
  }
}
