import 'package:ezv_app_tes/app/modules/products/components/empty_product.dart';
import 'package:ezv_app_tes/app/modules/products/components/item_product.dart';
import 'package:ezv_app_tes/app/modules/products/controllers/products_controller.dart';
import 'package:ezv_app_tes/design/colors.dart';
import 'package:ezv_app_tes/widgets/loading_indicator.dart';
import 'package:ezv_app_tes/widgets/page_base.dart';
import 'package:ezv_app_tes/widgets/page_refresher.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductsView extends GetView<ProductsController> {
  const ProductsView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return PageBase(
      title: 'Products',
      showIconBack: false,
      centeredTitle: true,
      child: Container(
        color: bgColor,
        child: PageRefresher(
          onRefresh: controller.getProducts,
          child: Obx(
            () => Visibility(
              replacement: Visibility(
                visible: controller.isLoading(),
                replacement: const EmptyProduct(),
                child: const Center(child: LoadingIndicator()),
              ),
              visible: controller.listProduct.isNotEmpty,
              child: ListView.builder(
                itemCount: controller.listProduct.length,
                itemBuilder: (c, i) => ItemProduct(
                  isLiked: controller.listProduct[i].isLiked,
                  onTap: () => controller.showDetail(controller.listProduct[i]),
                  item: controller.listProduct[i],
                  onTapLike: () => controller.like(controller.listProduct[i]),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
