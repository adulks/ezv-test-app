import 'package:cached_network_image/cached_network_image.dart';
import 'package:ezv_app_tes/app/models/product_model.dart';
import 'package:ezv_app_tes/app/modules/products/components/icon_favorite.dart';
import 'package:ezv_app_tes/design/styles.dart';
import 'package:ezv_app_tes/utils/format_currency.dart';
import 'package:ezv_app_tes/widgets/card_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ItemProduct extends StatelessWidget {
  const ItemProduct({
    Key? key,
    required this.item,
    required this.onTap,
    required this.onTapLike,
    required this.isLiked,
  }) : super(key: key);
  final ProductModel item;
  final Function() onTap;
  final Function() onTapLike;
  final bool isLiked;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: CardApp(
        padding: Paddings.sm,
        margin: Paddings.hv(8, 4),
        child: Row(
          children: [
            Stack(
              children: [
                CachedNetworkImage(
                  imageUrl: item.thumbnail,
                  width: .2.sw,
                  height: .2.sw,
                  fit: BoxFit.cover,
                ),
                IconFavorite(isFavorited: isLiked, onTap: onTapLike)
              ],
            ),
            16.horizontalSpace,
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    item.title,
                    style: TS.labelLarge,
                  ),
                  4.verticalSpace,
                  Text(
                    item.description,
                    style: TS.bodyMedium.copyWith(color: Colors.grey.shade800),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                  4.verticalSpace,
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        priceFormat(item.price),
                        style: TS.labelMedium.copyWith(
                          decoration: TextDecoration.lineThrough,
                          color: Colors.grey.shade800,
                        ),
                      ),
                      12.horizontalSpace,
                      Text(
                        priceFormat(
                          item.price -
                              ((item.discountPercentage / 100) * item.price),
                        ),
                        style: TS.labelMedium.copyWith(color: Colors.green),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
