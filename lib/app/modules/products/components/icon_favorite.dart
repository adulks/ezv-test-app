import 'package:ezv_app_tes/design/styles.dart';
import 'package:flutter/material.dart';

class IconFavorite extends StatelessWidget {
  const IconFavorite({Key? key, required this.isFavorited, required this.onTap})
      : super(key: key);
  final bool isFavorited;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: Paddings.xs,
        child: isFavorited
            ? const Icon(
                Icons.favorite,
                color: Colors.red,
              )
            : Icon(
                Icons.favorite_border,
                color: Colors.grey.shade800,
              ),
      ),
    );
  }
}
