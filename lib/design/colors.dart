import 'package:flutter/material.dart';

const primaryColor = Color(0xFFff7400);
const primaryDark = Color.fromARGB(255, 206, 94, 2);
const primaryAccent = Color(0xFFff7400);

const bgColor = Color.fromARGB(255, 240, 240, 240);
const bgColorDark = Color.fromARGB(255, 105, 105, 105);

const focusColor = Color.fromARGB(255, 0, 183, 58);
const errorColor = Color.fromARGB(255, 227, 49, 0);
const normalColor = Color.fromARGB(255, 177, 177, 177);
const disabledColor = Color.fromARGB(255, 149, 149, 149);

const appTextColor = Color.fromARGB(255, 27, 27, 27);

const white = Colors.white;

const Map<int, Color> materialColor = {
  50: Color.fromRGBO(136, 14, 79, .1),
  100: Color.fromRGBO(136, 14, 79, .2),
  200: Color.fromRGBO(136, 14, 79, .3),
  300: Color.fromRGBO(136, 14, 79, .4),
  400: Color.fromRGBO(136, 14, 79, .5),
  500: Color.fromRGBO(136, 14, 79, .6),
  600: Color.fromRGBO(136, 14, 79, .7),
  700: Color.fromRGBO(136, 14, 79, .8),
  800: Color.fromRGBO(136, 14, 79, .9),
  900: Color.fromRGBO(136, 14, 79, 1),
};
