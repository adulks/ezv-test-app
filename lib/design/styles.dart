import 'package:ezv_app_tes/app_config.dart';
import 'package:ezv_app_tes/design/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

ThemeData appTheme(BuildContext context) {
  return ThemeData(
    platform: TargetPlatform.iOS,
    brightness: Brightness.light,
    primaryColor: MaterialColor(primaryColor.value, materialColor),
    primarySwatch: MaterialColor(primaryColor.value, materialColor),
    fontFamily: fontFamilyApp,
    splashColor: Colors.white,
    scaffoldBackgroundColor: bgColor,
  );
}

ThemeData darkTheme(BuildContext context) {
  return ThemeData(
    platform: TargetPlatform.iOS,
    brightness: Brightness.light,
    primaryColor: MaterialColor(primaryDark.value, materialColor),
    primarySwatch: MaterialColor(primaryDark.value, materialColor),
    fontFamily: fontFamilyApp,
    splashColor: Colors.white,
    scaffoldBackgroundColor: bgColorDark,
    textTheme: Theme.of(context).textTheme.apply(
          bodyColor: Colors.white,
          displayColor: Colors.white,
        ),
  );
}

class Insets {
  static double get xs => 4.w;
  static double get sm => 8.w;
  static double get med => 12.w;
  static double get lg => 16.w;
  static double get xl => 20.w;
  static double get xxl => 32.w;
}

class IconSizes {
  static double get xs => 12.w;
  static double get sm => 20.w;
  static double get med => 28.w;
  static double get lg => 32.w;
  static double get xl => 48.w;
  static double get xxl => 64.w;
}

class Paddings {
  static EdgeInsets get xxs => EdgeInsets.all(2.w);
  static EdgeInsets get xs => EdgeInsets.all(4.w);
  static EdgeInsets get sm => EdgeInsets.all(8.w);
  static EdgeInsets get med => EdgeInsets.all(12.w);
  static EdgeInsets get lg => EdgeInsets.all(16.w);
  static EdgeInsets get xl => EdgeInsets.all(20.w);
  static EdgeInsets get xxl => EdgeInsets.all(32.w);

  static EdgeInsets hv(double h, double v) =>
      EdgeInsets.symmetric(horizontal: h, vertical: v);
}

class Corners {
  static double get xs => 4.w;
  static double get sm => 8.w;
  static double get med => 12.w;
  static double get lg => 16.w;
  static double get xl => 20.w;
  static double get xxl => 32.w;

  static Radius get xsRadius => Radius.circular(xs);
  static Radius get smRadius => Radius.circular(sm);
  static Radius get medRadius => Radius.circular(med);
  static Radius get lgRadius => Radius.circular(lg);
  static Radius get xlRadius => Radius.circular(xl);
  static Radius get xxlRadius => Radius.circular(xxl);

  static BorderRadius get xsBorder => BorderRadius.all(xsRadius);
  static BorderRadius get smBorder => BorderRadius.all(smRadius);
  static BorderRadius get medBorder => BorderRadius.all(medRadius);
  static BorderRadius get lgBorder => BorderRadius.all(lgRadius);
  static BorderRadius get xlBorder => BorderRadius.all(xlRadius);
  static BorderRadius get xxlBorder => BorderRadius.all(xxlRadius);
}

// Default Material Text Styles
class TS {
  static final color = Get.isDarkMode ? Colors.white : dTextColor;
  static TextStyle ts = TextStyle(
    fontFamily: fontFamilyApp,
    letterSpacing: 0,
    fontWeight: FontWeight.w400,
  );

  static TextStyle get displayLarge =>
      ts.copyWith(color: color, fontSize: 57.sp);
  static TextStyle get displayMedium =>
      ts.copyWith(color: color, fontSize: 45.sp);
  static TextStyle get displaySmall =>
      ts.copyWith(color: color, fontSize: 44.sp);

  static TextStyle get headlineLarge =>
      ts.copyWith(color: color, fontSize: 32.sp);
  static TextStyle get headlineMedium =>
      ts.copyWith(color: color, fontSize: 28.sp);
  static TextStyle get headlineSmall =>
      ts.copyWith(color: color, fontSize: 24.sp);

  static TextStyle get titleLarge =>
      ts.copyWith(color: color, fontSize: 22.sp, fontWeight: FontWeight.w600);
  static TextStyle get titleMedium => ts.copyWith(
        color: color,
        fontSize: 16.sp,
        fontWeight: FontWeight.w600,
        letterSpacing: .15,
      );
  static TextStyle get titleSmall => ts.copyWith(
        color: color,
        fontSize: 14.sp,
        fontWeight: FontWeight.w600,
        letterSpacing: .1,
      );

  static TextStyle get labelLarge => ts.copyWith(
        color: color,
        fontSize: 14.sp,
        fontWeight: FontWeight.w600,
        letterSpacing: .1,
      );
  static TextStyle get labelMedium => ts.copyWith(
        color: color,
        fontSize: 12.sp,
        fontWeight: FontWeight.w600,
        letterSpacing: .5,
      );
  static TextStyle get labelSmall => ts.copyWith(
        color: color,
        fontSize: 11.sp,
        fontWeight: FontWeight.w600,
        letterSpacing: .5,
      );

  static TextStyle get bodyLarge => ts.copyWith(
        color: color,
        fontSize: 16.sp,
        letterSpacing: .15,
      );
  static TextStyle get bodyMedium => ts.copyWith(
        color: color,
        fontSize: 14.sp,
        letterSpacing: .25,
      );
  static TextStyle get bodySmall => ts.copyWith(
        color: color,
        fontSize: 12.sp,
        letterSpacing: .4,
      );
}
