import 'package:ezv_app_tes/design/colors.dart';
import 'package:ezv_app_tes/widgets/appbar_default.dart';
import 'package:flutter/material.dart';

class PageBase extends StatelessWidget {
  const PageBase({
    Key? key,
    required this.child,
    this.appBar,
    this.resizeInsetsBottom = true,
    this.onBack,
    this.showIconBack = true,
    this.title = 'Appbar Title',
    this.centeredTitle = false,
    this.actions,
    this.bottomBar,
    this.bottomBarHeight,
  }) : super(key: key);
  final Widget child;
  final Widget? appBar;
  final bool resizeInsetsBottom;

  final Function()? onBack;
  final bool showIconBack;
  final String title;
  final bool centeredTitle;
  final List<Widget>? actions;
  final Widget? bottomBar;
  final double? bottomBarHeight;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryDark,
      resizeToAvoidBottomInset: resizeInsetsBottom,
      body: SafeArea(
        child: Column(
          children: [
            appBar ??
                AppbarDefault(
                  onBack: onBack,
                  showIconBack: showIconBack,
                  title: title,
                  centeredTitle: centeredTitle,
                  actions: actions,
                ),
            Expanded(child: child),
            if (bottomBar != null)
              SizedBox(
                height: bottomBarHeight,
                child: bottomBar,
              )
          ],
        ),
      ),
    );
  }
}
