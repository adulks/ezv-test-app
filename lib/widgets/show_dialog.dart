import 'package:ezv_app_tes/design/styles.dart';
import 'package:ezv_app_tes/widgets/ui_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

void showPopUpInfo({
  String? title,
  String? description,
  String? labelButton,
  String? imageUri,
  double? imageSize,
  bool? dismissible,
  Function()? onPress,
}) {
  Get.dialog(
    Dialog(
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.symmetric(horizontal: 40.sp),
      child: Container(
        padding: EdgeInsets.fromLTRB(
          Insets.med,
          Insets.xl,
          Insets.med,
          Insets.xs,
        ),
        decoration: BoxDecoration(
          borderRadius: Corners.smBorder,
          color: Colors.white,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              title ?? '',
              style: TS.titleMedium,
              textAlign: TextAlign.center,
            ),
            if (imageUri != null)
              Image.asset(
                imageUri,
                height: imageSize ?? IconSizes.xxl,
                width: imageSize ?? IconSizes.xxl,
              )
            else
              const SizedBox(),
            const SizedBox(height: 12),
            Text(
              description ?? '',
              style: TS.bodyMedium,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 28),
            UIButton(
              onPressed: () {
                Get.back();
                if (onPress != null) {
                  onPress();
                }
              },
              text: labelButton ?? 'OK',
              fullWidth: true,
            ),
            SizedBox(height: 20.h),
          ],
        ),
      ),
    ),
    barrierDismissible: dismissible ?? true,
  );
}
