import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PageRefresher extends StatefulWidget {
  const PageRefresher({Key? key, this.child, required this.onRefresh})
      : super(key: key);
  final Widget? child;
  final Function() onRefresh;

  @override
  State<PageRefresher> createState() => _PageRefresherState();
}

class _PageRefresherState extends State<PageRefresher> {
  RefreshController controller = RefreshController();
  Future<void> onRefreshing() async {
    await widget.onRefresh();
    controller.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: controller,
      onRefresh: onRefreshing,
      child: widget.child,
    );
  }
}
