import 'package:ezv_app_tes/app_config.dart';
import 'package:ezv_app_tes/design/colors.dart';
import 'package:ezv_app_tes/design/styles.dart';
import 'package:ezv_app_tes/widgets/card_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class AppbarDefault extends StatelessWidget {
  const AppbarDefault({
    Key? key,
    this.onBack,
    this.showIconBack = true,
    this.title = 'Appbar Title',
    this.centeredTitle = false,
    this.actions,
  }) : super(key: key);
  final Function()? onBack;
  final bool showIconBack;
  final String title;
  final bool centeredTitle;
  final List<Widget>? actions;

  @override
  Widget build(BuildContext context) {
    return CardApp(
      padding: Paddings.hv(Insets.med, 0),
      height: dAppbarHeight,
      color: primaryColor,
      margin: EdgeInsets.zero,
      width: Get.width,
      radius: 0,
      child: Stack(
        alignment: Alignment.center,
        children: [
          if (showIconBack)
            Positioned(
              left: 0,
              top: 0,
              bottom: 0,
              child: InkWell(
                onTap: onBack ?? Get.back,
                child: const Padding(
                  padding: EdgeInsets.all(8),
                  child: Icon(Icons.arrow_back, color: Colors.white),
                ),
              ),
            ),
          if (centeredTitle)
            Text(
              title,
              style: TS.titleMedium.copyWith(color: Colors.white),
              textAlign: TextAlign.center,
            )
          else
            Container(
              width: Get.width,
              padding:
                  showIconBack ? EdgeInsets.only(left: 48.w) : EdgeInsets.zero,
              child: Text(
                title,
                style: TS.titleMedium.copyWith(color: Colors.white),
                textAlign: TextAlign.start,
              ),
            ),
          if (actions != null)
            Positioned(
              bottom: 0,
              right: 0,
              top: 0,
              child: Row(
                children: actions!,
              ),
            )
        ],
      ),
    );
  }
}
