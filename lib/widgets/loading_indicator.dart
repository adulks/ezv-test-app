import 'package:ezv_app_tes/design/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator({Key? key, this.color, this.size}) : super(key: key);
  final Color? color;
  final double? size;

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: size ?? 20.w,
      child: CircularProgressIndicator(
        color: color ?? primaryColor,
        strokeWidth: 2,
      ),
    );
  }
}
