import 'package:ezv_app_tes/app_config.dart';
import 'package:flutter/material.dart';

class CardApp extends StatelessWidget {
  const CardApp({
    Key? key,
    this.width,
    this.height,
    this.radius,
    this.borderWidth = dCardBorderWidth,
    this.color = dCardColor,
    this.outlineColor = dCardOutlineColor,
    this.isOutlined = dCardIsOutline,
    this.margin,
    this.padding,
    this.child,
    this.shadows,
    this.isShadow = false,
  }) : super(key: key);
  final double? width;
  final double? height;
  final double? radius;
  final double borderWidth;
  final Color color;
  final Color outlineColor;
  final bool isOutlined;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final Widget? child;
  final List<BoxShadow>? shadows;
  final bool isShadow;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: margin ?? dCardMargin,
      padding: padding,
      decoration: isOutlined
          ? BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(radius ?? dCardRadius),
              ),
              border: Border.all(color: outlineColor, width: borderWidth),
              color: color,
              boxShadow: isShadow ? shadows ?? dShadows : [],
            )
          : BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(radius ?? dCardRadius),
              ),
              color: color,
              boxShadow: isShadow ? shadows ?? dShadows : [],
            ),
      child: child,
    );
  }
}
